\subsection{CSV/CSV.lhs}

This module was provided to us by Dr. Andrew Rock. None of it is my independent work and it is only included here for completeness.
For a complete run-down of the Grammar, see sections 2.1 and 2.2.

\begin{code}
module CSV.CSV where
\end{code}

\begin{code}
import ABR.Parser
\end{code}

\subsubsection{Lexer}

\begin{code}
commaL :: Lexer
commaL = literalL ','
\end{code}

\begin{code}
eolL :: Lexer
eolL = literalL '\n'
\end{code}

\begin{code}
fieldL :: Lexer
fieldL = 
      (many (satisfyL (const True) "" `alsoNotSat` separatorL) &%> "F")
   <& commaL
\end{code}

\begin{code}
lastFieldL :: Lexer
lastFieldL = 
      (many (satisfyL (const True) "" `alsoNotSat` separatorL) &%> "LF")
   <& eolL
\end{code}

\begin{code}
quotedFieldL :: Lexer
quotedFieldL = 
   (     literalL '"'
    <&&> (many (    satisfyL (const True) "" `alsoNotSat` literalL '"'
                <|> tokenL "\"\"")
            &%> "")
    <&&> nofail (literalL '"')
    <& commaL
   ) %> "QF"
\end{code}

\begin{code}
lastQuotedFieldL :: Lexer
lastQuotedFieldL = 
   (     literalL '"'
    <&&> (many (    satisfyL (const True) "" `alsoNotSat` literalL '"'
                <|> tokenL "\"\"")
            &%> "")
    <&&> nofail (literalL '"')
    <& eolL
   ) %> "LQF"
\end{code}

\begin{code}
separatorL :: Lexer
separatorL = 
       commaL
   <|> eolL
\end{code}


\begin{code}
lexerL :: Lexer
lexerL = listL [quotedFieldL, fieldL, lastQuotedFieldL, lastFieldL]
\end{code}

\subsubsection{Data types}

\begin{code}
type Cell = String
type Row = [Cell]
type Table = [Row]
\end{code}

\subsubsection{Parser}

\begin{code}
cellP :: Parser Cell
cellP = 
   (tagP "QF" <|> tagP "F")
   @> (\(_, c, _) -> c)
\end{code}

\begin{code}
lastCellP :: Parser Cell
lastCellP = 
   (tagP "LQF" <|> tagP "LF")
   @> (\(_, c, _) -> c)
\end{code}

\begin{code}
rowP :: Parser Row
rowP = many cellP <&> lastCellP
       @> (\(cs,c) -> cs ++ [c])
\end{code}

\begin{code}
tableP :: Parser Table
tableP = many rowP
\end{code}
