\documentclass[a4paper]{article}

\usepackage{HaskellVerb}
\usepackage{FillAFour}
\usepackage{alltt}
\usepackage{color}
\usepackage{epsfig}
\usepackage{url}
\usepackage{epstopdf}
\usepackage{soul}
\usepackage[english]{babel}
\usepackage[pdftex, a4paper, backref, colorlinks, bookmarksnumbered=true]{hyperref}

\DefineVerbatimEnvironment{ebnf}{Verbatim}{frame=single, rulecolor=\color{blue}}

\newcommand{\ACESD}{{\sc ACESD}}

\title{CSVJoin}
\author{Thanasi Poulos\\ s2703592}

\begin{document}
\maketitle
\begin{abstract}
This paper contains the usage instructions and complete source code of the program {\em CSVJoin} which was produced as a part of the Functional Programming honours course at Griffith University.
It aims to facilitate quick marks uploading to L@G by joining the marker's csv files with the L@G csv file and producing an output that L@G will accept.
\end{abstract}
\clearpage
\tableofcontents
\clearpage
\section{Introduction}
This project involved writing a Haskell program that allowed a "Join" between CSV files, in order to facilitate easy updating of marks on Learning@Griffith's marks centre.
In order to achieve this, we first needed to identify CSV's grammar, in order to build a Lexer/Parser library. The program needed to be configurable using an external config file of some kind.
"Joining" the tables involves a few things. First, we need to identify which column in the CSVs contains the keys (we know the column headings from the configs). 
After this, we need to match the keys in the so-called "truth" file, to the keys in the file that originates from L@G.
In doing so, we need to, row-by-row, update the "join columns" (also in the configs) in the L@G file with the values from the "truth".
The join columns, and the key columns, are specified in the config file.
Once this join is complete, the program needs to output a file with the correct column names (required by L@G) so it can be uploaded.
\\\\
In order to achieve all this, a fairly comprehensive set of functions needed to be built, to allow us to convert the "Table" (in actual fact, a [[String]]) to Arrays/IOArrays and back,
to calculate all the intermediate products needed to perform the join,
to reconvert back to a "Table" once all the modifying had been done and finally
to write the "merged" CSV back to a file.
\clearpage
\section{High Level Design}
\subsection{Lexers}
In order to parse the CSV files correctly, we needed to implement the CSV grammar using the ABR Parsers and Lexers. They are presented in this section in both EBNF and using Syntrax diagrams.
\\
\subsubsection{Comma}
A {\em Comma} is an important character in a "Comma Separated Values" file, so it has its own Lexer. It is merely a comma character literal
\begin{ebnf}
comma ::= ",";
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/comma.eps}
\end{center}

\subsubsection{End Of Line}
We need to be able to detect the end of a line when lexing. Additionally, the end of the file is a special case of the end of a line.
\begin{ebnf}
eol ::= "\\n" | $end of file$; 
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/eol.eps}
\end{center}

\subsubsection{Separator}
A {\em Separator} is what defines the boundaries of each value (or cell) within the CSV file. This is either a {\em Comma} or an {\em Eol}.
\begin{ebnf}
separator ::= comma | eol;
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/separator.eps}
\end{center}

\clearpage

\subsubsection{Field}
A {\em Field} is anything that is NOT a {\em Separator}, followed by a {\em Comma}. This is what contains the data.
\begin{ebnf}
field ::= {<$anything$ ! separator>} comma;
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/field.eps}
\end{center}

\subsubsection{Last Field}
A {\em Last Field} is a special case of a {\em Field}. It is anything that is NOT a {\em Separator}, followed by an {\em Eol}. Recall that an {\em Eol} may also be the end of the file.
\begin{ebnf}
lastField ::= {<$anything$ ! separator>} eol;
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/lastField.eps}
\end{center}

\subsubsection{Quoted Field}
A {\em Quoted Field} is a {\em Field} in which there exist quote ("") characters. It must start and end with a quote character (before the {\em Comma}). It may also contain quotes, but ony in pairs.
It is a special case of {\em Field}, because it may contain {\em Separator}s ({\em Comma}s and {\em Eol}s) within any pair of quotes.
\begin{ebnf}
quotedField ::= "\"" {<$anything$ ! "\""> | "\"\""} "\"" comma;
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/quotedField.eps}
\end{center}

\subsubsection{Last Quoted Field}
A special case of {\em Quoted Field} where there is an {\em Eol} instead of a {\em Comma}.
\begin{ebnf}
quotedField ::= "\"" {<$anything$ ! "\""> | "\"\""} "\"" eol;
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/lastQuotedField.eps}
\end{center}

\subsubsection{CSV}
Finally, a CSV file can be defined. A CSV file is made up of {\em Fields}, {\em Quoted Fields}, {\em Last Fields} and {\em Last Quoted Fields}.
\begin{ebnf}
lexer ::= {quotedField | field | lastQuotedField | lastField}; 
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/lexer.eps}
\end{center}
\clearpage

\subsection{Parsers}
The following is the Parser grammar.
\subsubsection{Cell}
A {\em Cell} is either a {\em Field} or a {\em Quoted Field}.
\begin{ebnf}
cell ::= quotedField | field.
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/cell.eps}
\end{center}

\subsubsection{Last Cell}
A {\em Last Cell} is either a {\em Last Field} or a {\em Last Quoted Field}.
\begin{ebnf}
lastCell ::= lastQuotedField | lastField.
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/lastCell.eps}
\end{center}

\subsubsection{Row}
A {\em Row} is a set of {\em Cell}s, followed by one {\em Last Cell}.
\begin{ebnf}
row ::= {cell} lastCell.
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/row.eps}
\end{center}

\subsubsection{Table}
Finally, a {\em Table} is a set of {\em Row}s.
\begin{ebnf}
table ::= {row}.
\end{ebnf}
\begin{center}
\includegraphics[scale=1.5]{img/table.eps}
\end{center}
\clearpage

\subsection{Program Design}
Algorithmically, my intended process was fairly straightforward. Once I had parsed the configs, then used those configs to parse the "truth" and Learning@Griffith CSVs,
I would perform the following steps:
\begin{enumerate}
	\item Convert the parsed "Truth" table into an Immutable Array,
	\begin{itemize}
		\item The "Truth" does not ever get modified, so it is not necessary to go beyond this
	\end{itemize}
	\item Convert the parsed Learning@Griffith table into an Immutable Array,
	\item Convert that Array to an IOArray,
	\begin{itemize}
		\item This is the CSV we want to perform the join into
	\end{itemize}
	\item Perform the Join, which involves:
	\begin{itemize}
		\item Getting the list of Join Columns from the configs,
		\item Working out which columns these are within the CSVs,
		\item Getting the names of the Key Coulmns from the configs,
		\item Working out which columns those are within the CSVs and finally,
		\item Using the list of Merge Columns and the known Key Columns, update each of the Merge Colums by comparing
		the values in the Key Columns and copying the data from the "Truth" into the Learning@Griffith file
	\end{itemize}
	\item Convert the now modified IOArray back to an Immutable Array,
	\item Convert that Array back into a Table and finally,
	\item Write the output file with the modified Learning@Griffith CSV values.
\end{enumerate}
\clearpage
\section{Implementation}
\input{CSV/CSV.lhs}
\clearpage
\input{CSVJoin.lhs}
\clearpage
\section{Usage}
\subsection{Execution}
Usage is very simple. This is a command-line utility, executed like:
\begin{center}
./CSVJoin {\em config file}
\end{center}
The config file does not need to have any particular extension, as long as it is plaintext. You must have write permission in the directory you execute the file in.
\subsection{Config File Layout}
Here is an example of a config file for \texttt{\hl{CSVJoin}}:
\begin{code}
# Configuration File for CSVJoin

# Output file config
outputFile = {
    name = merged.csv
}

# Truth file config
truthFile = {
    name = truth.csv
    key =  "Student ID"
}

# lag file config
lagFile = {
    name = lag.csv
    key = "Heading 1"
}

# Columns to merge
mergeCols = [
    truthCol = "Lab 1"
    lagCol = "Mark 1",
    truthCol = "Lab 2"
    lagCol = "Mark 2"
]
\end{code}
\begin{itemize}
\item outputFile
\begin{itemize}
	\item name: The filename you wish to save the merged CSV with
\end{itemize}
\item truthFile
\begin{itemize}
	\item name: The filename for the "truth" file
	\item key: The column heading for the column that contains the student keys
\end{itemize}
\item lagFile
\begin{itemize}
	\item name: The filename for the Learning@Griffith file
	\item key: The column heading for the column that contains the student keys
\end{itemize}
\item mergeCols
\begin{itemize}
	\item truthCol: The name of the first column to merge from the "truth"
	\item lagCol: The name of the first column to merge from the Learning@Griffith file
	\begin{itemize}
		\item This is a List of configs, each pair of (truthCol,lagCol) indicates which column in the "truth" aligns with which column in the Learning@Griffith file.
		This can theoretically be an infinitely long list.
	\end{itemize}
\end{itemize}
\end{itemize}
\clearpage
\section{Results}
My implementation was mostly successful, though there are still a few fairly big bugs. The functionality required of the application exists in its entirety
(as far as joining columns on student numbers) however my application does not cover some fairly important corner cases.
First of all, if the program runs off the end of the list of student numbers, it does not fail gracefully and continue, it merely dies.
Secondly, if a key in the "truth" is not found in the Learning@Griffith file, the program halts execution.
It does not deal with CSVs with a different number of rows.
I have provided some small sample files on which it works, to demonstrate that it does indeed merge the columns correctly (even correctly dealing with non-contiguous merge columns),
however outside of these files, I have had limited success.
\section{Reflection}
\subsection{On the course}
I have had an interest in learning about functional programming for quite some time now; ever since a friend of mine introduced me to the concept a couple of years ago.
I had wanted to take this course last year, however time did not permit me to do so. I have, in recent years, become more accustomed to thinking mathematically as I have taken
a liking to mathematics in general, so this course seemed like a good fit. I have enjoyed the learning experience, both the theoretical and practical, however I found myself
often wanting to delve more deeply into the theory rather than the many practical examples. I understand that we needed to learn a certain amount fairly quickly so we could
start our projects, and that potentially it was the overall seemingly slow progress of the students this semester that caused us to focus so much on practical aspects, however I
still feel more theoretical discussion would've been beneficial. Otherwise however, I feel like this has been a fruitful foray into the world of Functional Programming and I find
myself much more convinced of ts utility than I have been in the past.
\subsection{On the process}
This was both positive and negative for me. I found myself often extremely frustrated and losng motivation when faced with something seemingly simple that I could not
for the life of me get working Functionally. Specifically, I had a lot of trouble thinking about recursion as a means of repeating processes; I have never really written
anything that uses an extensive amount of recursion so I often found myself sitting at my desk, staring at the screen drawing little figures in the air with my fingers
trying to work out what was happening. One could argue that this meshes nicely with my liking of mathematics as a lot of time there is also spent just working through things
mentally, but I have become accustomed to producing results when programming and often finding out that a solution to something I lost an entire day on is a single line of code 
(READ: LIST COMPREHENSIONS), was more discouraging than encouraging. Overall however, ultimately I found the learning process was fun and interesting and I appreciate
being given the opportunity to gain an insight into such a interesting field.
\subsection{On the project}
Being given the code for the CSV lexer and parser on one hand seems like a missed opportunity, but by the same token I can see us having made little progress without it.
Along with some other concepts like compilers, parsing and lexing has always been something I've wanted to study in more detail, however I understand that the time constraints
at university probably made it idifficult to fit in an real discussion about these topics. The project itself was something I would consider fairly straightforward were I
implementing it in a language I am familiar with; it was anything but in Haskell. Having no realy experience short of a few days with a friend who showed me the basics,
I found myself struggling to even understand {\em how} something might work, let alone how I would implement it. This was most true of anything that required iteration;
as I mentioned before, I am unfamiliar with recursive strategies and lost a lot of time trying to work out how to iterate over arrays/lists, how to pass objects around
in the form I wanted to and often even found myself having difficulty coming up with a robust type signature for a function. It got easier towards the end and I must say,
there was a point at which list comprehensions kinda "clicked" and from that point forward, armed just with those, I became a fair amount more effective. Overall the project
was fun, interesting and a good learning experience; it kind of leaves me wishing there was a follow-up subject that could help us consolidate the things we have learned through
our many day of trial-and-error programming and frustration.
\end{document}
