\subsection{CSVJoin.lhs}

\subsubsection{Imports}

The following modules were used for the implementation of the project. This project made extensive use of Dr. Andrew Rock's ABR libraries, for most of the functionality including Parsing/Lexing of configs and String Manipulation.
\begin{code}
module Main (main) where

import System.Environment
import Data.Array
import Data.List
import Data.Array.IO
import Data.Maybe

import ABR.Control.Check
import ABR.Parser
import ABR.Parser.Checks
import ABR.Text.Configs
import ABR.Text.String

import CSV.CSV
\end{code}

\subsubsection{Type Synonyms}
The following type synonyms were used in order to make the implementation more readable:
\begin{code}
type ATable = Array (Int, Int) String
\end{code}
This type synonym describes an Immutable array of Strings, indexed by an Int-tuple. We call this an ATable, as this is the data structure we store our Table in and it is a regular Immutable array.
\begin{code}
type MTable = IOArray (Int, Int) String
\end{code}
This type synonym describes a Mutable IOArray of Strings, indexed by an Int-tuple. This is called an MTable because it is a Table stored in a Mutable array.
\begin{code}
type ColHeader = String
\end{code}
This type synonym exists solely to give context to functions which take a Column Header.

\subsubsection{Table/Array Manipulation}
This section of the implementation deals with the functions created in order to manipulate the parsed tables, including converting Tables (which are merely [[String]]s) to and from Arrays/IOArrays as well as turning the Table back into a CSV and writing it to a file.

\begin{code}
table2Csv :: Table -> String
table2Csv t = unlines (map (\r -> concat (intersperse "," r)) t)
\end{code}
\texttt{\hl{table2Csv}} {\em t} takes a Table and turns it into a String that is the exact CSV file our Table represented.

\begin{code}
writeTbl2File :: FilePath -> Table -> IO()
writeTbl2File f t = writeFile f (table2Csv t)
\end{code}
\texttt{\hl{writeTbl2File}} {\em f t} takes a FilePath and a Table and simply writes a file with the contents of the table (after converting to CSV).

\begin{code}
table2array :: Table -> ATable
table2array t = array ((0, 0), (length t - 1, length (head t) - 1))
   [((i, j), c) | (i, r) <- zip [0..] t, (j, c) <- zip [0..] r]
\end{code}
\texttt{\hl{table2array}} {\em t} takes a Table and turns it into an ATable (Array (Int, Int) String).

\begin{code}
array2mutableArray :: ATable -> IO MTable
array2mutableArray = thaw
\end{code}
\texttt{\hl{array2mutableArray}} simply "thaws" the ATable (a regular Array) and turns it into an MTable (an IOArray).

\begin{code}
mutableArray2array :: MTable -> IO ATable
mutableArray2array = freeze
\end{code}
\texttt{\hl{array2mutableArray}} does the opposite of the above.

\begin{code}
updateMTable :: MTable -> (Int, Int) -> String -> IO ()
updateMTable a i e = writeArray a i e
\end{code}
\texttt{\hl{updateMTable}} {\em a i e} is just a wrapper for writeArray to give it a more relevant name

\begin{code}
updateChunkOfMTable :: MTable -> (Int, Int) -> ATable -> IO ()
updateChunkOfMTable m i e = sequence_ 
   [writeArray m (x + (fst i),y + (snd i)) a | ((x,y),a) <- assocs e]
\end{code}
\texttt{\hl{updateChunkOfMTable}} {\em m i e} takes an MTable, a position and an ATable and inserts the ATable into the MTable at the position.

\begin{code}
groupInto :: Int -> [a] -> [[a]]
groupInto n xs = case xs of
   [] -> []
   (_:_) -> let (ys, xs') = splitAt n xs in 
      ys : groupInto n xs'
\end{code}
\texttt{\hl{groupInto}} {\em n xs} is one part of the \texttt{\hl{array2table}} process, which takes an Int (row width) and a [a] and turns it into a [[a]] where each [a] is {\em n} wide.

\begin{code}      
array2table :: ATable -> Table
array2table a = groupInto ((snd (snd (bounds a))) + 1) (elems a)
\end{code}
\texttt{\hl{array2table}} {\em a} takes an ATable and returns a Table, using the \texttt{\hl{groupInto}} function.

\subsubsection{Update/Utility functions}
\begin{code}
cListLength :: Config -> Maybe Int
cListLength c = case c of
   CList _ cs ->
      Just (length cs)
   _ ->
      Nothing
\end{code}
\texttt{\hl{cListLength}} {\em c} takes a CList object and tells you how many items it has in it.

\begin{code}
updateColumn :: Int -> Int -> Int -> Int -> ATable -> MTable -> ATable -> IO ()
updateColumn tkc lkc tmc lmc truth lag lag' = sequence_
    [writeArray lag ((x),lmc) a | (x, a) <- xas]
    where xs = [findRow tk lkc truth lag' | tk <- getCol tkc truth]
          as = getCol tmc truth
          xas = zip xs as
\end{code}
\texttt{\hl{updateColumn}} {\em tkc lkc tmc lmc truth lag lag'} takes 4 Integers, each one being a column index, the "truth" Table, and both an Immutable and Mutable version of the L@G Table.
The 4 Integers are, in order:
\begin{enumerate}
\item Truth Key Column
\item L@G Key Column
\item Truth Merge Column
\item L@G Merge Column
\end{enumerate}
With these, it sequences the operations needed to update the table as a series of calls to \texttt{\hl{writeArray}},
drawing the row numbers from a comparison between every value in the "truth" key column against the values in the L@G key column
and drawing the values to update with from the "truth" using \texttt{\hl{getCol}} {\em tmc truth}.

\begin{code}
updateColumnNames :: String -> String -> MTable -> ATable -> IO ()
updateColumnNames tc lc lagM lagA = 
    writeArray lagM (0, fromJust (findColumn lagA lc)) tc
\end{code}
\texttt{\hl{updateColumnNames}} {\em tc lc lagM lagA} was intended to copy the column names from the "truth" into the L@G file, before I realised that is not necessary.
I kept the function however, as it may have some use in future extensions to this project (if any).

\begin{code}
getCol :: Int -> ATable -> [String]
getCol i a = getStringFromAssocs (filter f (assocs a))
    where f ((x,y),_) = (x > 0) && (y == i)
\end{code}
\texttt{\hl{getCol}} {\em i a} takes an ATable {\em a} and returns the column at index {\em i}, minus the heading.

\begin{code}
getStringFromAssocs :: [((Int,Int),String)] -> [String]
getStringFromAssocs a = map snd a
\end{code}
\texttt{\hl{getStringFromAssocs}} {\em a} simply returns the second part of the assocs tuple, which is a String in our case.
I used this in combination with a filter to get a specific column of values, otherwise this is equivalent to using \texttt{\hl{elems}}.

\begin{code}
findRow :: String -> Int -> ATable -> ATable-> Int
findRow tk lkc truth lag = fst (fst (head (filter f (assocs lag))))
    where f ((_,_),x) = matchIDs x tk
\end{code}
\texttt{\hl{findRow}} {\em tk lkc truth lag} takes a String input (a key from he "truth") and tries to match it to any of the keys in the L@G CSV.

\begin{code}
findColumn :: ATable -> ColHeader -> Maybe Int
findColumn a c = findColumn' (getFirstRow a) c
\end{code}
\texttt{\hl{findColumn}} {\em a c} calls \texttt{\hl{findColumn'}} on the first row of an ATable to get the column index of that column.

\begin{code}
findColumn' :: [((Int,Int),ColHeader)] -> ColHeader -> Maybe Int
findColumn' al c = case al of
   [] -> Nothing
   ((_,col),ch):als -> if c == ch then Just col else findColumn' als c
\end{code}
\texttt{\hl{findColumn'}} {\em al c} searches through the column headers recursively until it finds the one you want (or not) and returns its index (or not).

\begin{code}
getFirstRow :: ATable -> [((Int,Int),ColHeader)]
getFirstRow a = filter f (assocs a)
   where f ((x,_),_) = x == 0
\end{code}
\texttt{\hl{getFirstRow}} {\em a} does what it says on the tin. Returns the first row of a Table, which for the purposes of this project, is assumed always to be a list of column headings.

\begin{code}
dropQuotes :: String -> String
dropQuotes = init . tail
\end{code}
\texttt{\hl{dropQuotes}} is named slightly awkwardly. I only use it to drop quotes, however in actual fact it just drops the first and last character, no matter what they are.

\begin{code}
getPairs :: Config -> Maybe [(String,String)]
getPairs c = case c of
    (CList _ css) -> Just [(getP "truthCol" cs, getP "lagCol" cs) | cs <- css]
    _ -> Nothing
\end{code}
\texttt{\hl{getPairs}} {\em c} takes a CList and gives back the tuple of "truth" merge column names and Learning@Griffith merge column names.

\begin{code}
csvJoin :: ATable -> MTable -> Configs -> IO ()
csvJoin t m cs = do
    let keyCols = lookupConfig "mergeCols" cs
    lag <- mutableArray2array m
    let truthKeyCol = fromJust (findColumn t (getP "truthFile.key" cs))
    let lagKeyCol = fromJust (findColumn lag (getP "lagFile.key" cs))
    let pairs = getPairs (fromJust keyCols)
    let truthMergeCols = map fst (fromJust pairs)
    let lagMergeCols = map snd (fromJust pairs)
    let tMergeIs = map fromJust [findColumn t n | n <- truthMergeCols]
    let lMergeIs = map fromJust [findColumn lag n | n <- lagMergeCols]
    let mergeIs = zip tMergeIs lMergeIs
    sequence_ [updateColumn truthKeyCol lagKeyCol tmc lmc t m lag | 
        (tmc,lmc) <- mergeIs]
\end{code}
\texttt{\hl{csvJoin}} {\em t m cs} is where the magic happens. The full process for this function is more-or-less what I specified in section 2.3, point 4.

\begin{code}
getP :: String -> Configs -> String
getP s cs = dropQuotes (getParam s cs)
\end{code}
\texttt{\hl{getP}} {\em s cs} wraps \texttt{\hl{getParam}} with dropQuotes, if I need to get a parameter without quotes around it.

\begin{code}
matchIDs :: String -> String -> Bool
matchIDs a b = do
    let left = if head a == 's' then rJustify' '0' 9 (tail a) else rJustify' '0' 9 a
    let right = if head b == 's' then rJustify' '0' 9 (tail b) else rJustify' '0' 9 b
    if (left == filter (\c -> c >= '0' && c <= '9') left) && 
        (right == filter (\c -> c >= '0' && c <= '9') right) && 
        left == right then True else False
\end{code}
\texttt{\hl{matchIDs}} {\em a b} takes two Strings (which are intended to be student numbers), checks if they have an s on the front, drops it, pads the strings out to 9 characters
(with zeroes), checks that they are still numbers and then checks if they match.
\clearpage
\subsubsection{Main}
\begin{code}
main :: IO ()
main = do
   [arg] <- getArgs
   config <- readFile arg
   case checkParse configsL (nofail (total configsP)) config of
      CheckPass c -> do
         -- Parse truth.csv from configs
         truth <- readFile (getParam "truthFile.name" c)
         case checkParse lexerL (nofail (total tableP)) truth of
            CheckPass t -> do
               -- Parse lag.csv from configs
               lag <- readFile (getParam "lagFile.name" c)
               case checkParse lexerL (nofail (total tableP)) lag of
                  CheckPass l -> do
                     let truthA = table2array t
                     let lagA = table2array l
                     lagM <- array2mutableArray lagA
                     csvJoin truthA lagM c
                     lagMod <- mutableArray2array lagM
                     let lagModTbl = array2table lagMod
                     let lagModCSV = table2Csv lagModTbl
                     writeTbl2File (getParam "outputFile.name" c) lagModTbl
                     print "Success!"
                  CheckFail msg ->
                     putStrLn msg
                  -- END LAG FILE PARSE

            CheckFail msg ->
               putStrLn msg
            -- END TRUTH FILE PARSE

      CheckFail msg ->
         putStrLn msg
      -- END CONFIGS PARSE
\end{code}
\texttt{\hl{main}} functions as described in 2.3